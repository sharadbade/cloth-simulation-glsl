# Cloth Simulation Using Shaders

## Final project for CS-5229, Fall 2016, University of Colorado Boulder

### Sources Consulted:

1. Physics for game developers, First Edition
2. Verlet Integration method: https://en.wikipedia.org/wiki/Verlet_integration
    - I chose to do this method after reading about it on: http://gamedev.stackexchange.com/questions/15708/how-can-i-implement-gravity
3. OpenGL framebuffer tutorial: http://www.swiftless.com/tutorials/opengl/framebuffer.html
4. Render to texture mechanism: http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-14-render-to-texture/
5. Multiple Render Targets: http://stackoverflow.com/questions/7207422/setting-up-opengl-multiple-render-targets
6. http://www.codinglabs.net/tutorial_simple_def_rendering.aspx
7. Basic light source code is from CS5229 examples
8. Basic shader code is from CS5229 Shader lecture examples
9. Ping-pong technique example http://www.pixelnerve.com/v/2010/07/20/pingpong-technique/

### What was accomplished:
1. Full cloth simulation on CPU with sphere collision.
2. Full cloth simulation on GPU with sphere collision.
3. Learned using vertex buffer objects with shaders.
4. Learned "render to texture" technique for GPU computation.
5. Learned the usage of frame buffers.
6. Learned how to do particle physics in openGL.

### Challanges faced:
1. Learned a lot about framebuffer objects and how shaders work with it. This was not covered in class.
2. My main challange was get the shader code running correcly.
Since I did not know any debugging tools for shader code, I had to do lot of googling to remove bugs from my code.

### What to observe:
1. Number of iterations of the physics calculations dictates the smoothness of the simulations, but it comes at a certain cost. This problem can be solved by doing
the simulation on GPU.
2. So, if you increase the simulation iteration (say 20 which is max for my sim) and cloth dimensions (say 64x64), FPS drops considerably for CPU simulation, but GPU is able to maintain higher FPS, almost the double.
2. For small iterations (say 4) and cloth dimensions (say 16x16), FPS does not differ much between CPU and GPU sim, infact doing simulation on GPU is not efficient at this data point.

Note: I have included a graph of CPU/GPU computation Frame rate in a pdf file: project_graph_fps.pdf

### How to run:
1. unzip project_sharad_bade.zip
2. make clean && make
2. ./run
3. Use left and right arrow to move the scene around.
4. press m to toggle cloth simulation between CPU and GPU.

### Key Controls:
1.  Arrow keys to move the scene around.
2.  a/A: Toggle axes.
3.  v/V: Pause the movement of light.
4.  m/M: Toggle the CPU/GPU modes
5.  s: Decrease cloth stiffness
6.  S: Increase cloth stiffness
7.  d: Decrease cloth dimensions (multile of 2 and symetric)
8.  D: Increase cloth dimensions (multile of 2 and symetric)
9.  i: Decrease simulation iterations
10. I: Increase simulation iterations
11. c/C: Draw a colliding sphere
12. w: Draw wireframe of the cloth
12. spacebar: pause simulation
13. 0: reset

### Simulation Demo
[cloth simulation glsl](https://youtu.be/HV4-4bcy_Bo)
