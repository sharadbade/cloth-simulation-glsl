/*
Copyright (c) 2016, Sharad Bade
All rights reserved.
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list
of conditions and the following disclaimer in the documentation and/or other
materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.
*/

#include "Cloth.h"
#include "CSCIx229.h"

// define global variables
int th = -30; // Azimuth view angle
int ph = 10; // elevation view angle
int zh = 120; // Light azimuth
int axes = 1; // toggle axes
int asp = 1; // aspect ratio
int proj = 0; // project type
int fov=55; // field of view
int move = 1; // move the light source
float Ylight=0.5;   //  Light elevation
double dim = 1.2;
float stiffness = 55000;
float damping = 0.25;
float mass = 32;
float time_step = 0.01;
// Cloth width and height
int sim_iter = 4;
int cloth_width = 32;
int cloth_height = 32;
float inverse_mass_per_particle = 64.0;
float inverse_mass = inverse_mass_per_particle/float(cloth_width*cloth_height);
int mode = 0; // CPU = 0, GPU = 1
int pause_sim = 0;
int wireframe_mode = 0;
int fbo_select = 0;
Cloth cloth;
int shader[2] = {0, 0};
unsigned int texture[2]; //texture names
static GLint Frames = 0;
static GLfloat fps = -1;
static GLint T0 = 0;
int sphere = 0;


void display()
{
       const double len=1.0;  //  Length of axes
    //  Light position and colors
    float Emission[]  = {0.0,0.0,0.0,1.0};
    float Ambient[]   = {0.7,0.7,0.7,1.0};
    float Diffuse[]   = {1.0,1.0,1.0,1.0};
    float Specular[]  = {0.3,0.3,0.3,1.0};
    float Position[]  = {1.2*Cos(zh),Ylight,1.2*Sin(zh),1.0};
    float Shinyness[] = {16};

    //  Erase the window and the depth buffer
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    //glClearColor(1.f, 1.f, 1.0f, 1.0f);

    //  Enable Z-buffering in OpenGL
    glEnable(GL_DEPTH_TEST);
    //  Undo previous transformations
    glLoadIdentity();
    //  Perspective - set eye position
    if (proj)
    {
      double Ex = -2*dim*Sin(th)*Cos(ph);
      double Ey = +2*dim        *Sin(ph);
      double Ez = +2*dim*Cos(th)*Cos(ph);
      gluLookAt(Ex,Ey,Ez , 0,0,0 , 0,Cos(ph),0);
    }
    //  Orthogonal - set world orientation
    else
    {
      glRotatef(ph,1,0,0);
      glRotatef(th,0,1,0);
    }

    //  Draw light position as sphere (still no lighting here)
    glColor3f(1,1,1);
    glPushMatrix();
    glTranslated(Position[0],Position[1],Position[2]);
    glutSolidSphere(0.03,10,10);
    glPopMatrix();
    //  OpenGL should normalize normal vectors
    //glEnable(GL_NORMALIZE);
    //  Enable lighting
    glEnable(GL_LIGHTING);
    //  glColor sets ambient and diffuse color materials
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
    //  Enable light 0
    glEnable(GL_LIGHT0);

    //  Set ambient, diffuse, specular components and position of light 0
    glLightfv(GL_LIGHT0,GL_AMBIENT ,Ambient);
    glLightfv(GL_LIGHT0,GL_DIFFUSE ,Diffuse);
    glLightfv(GL_LIGHT0,GL_SPECULAR,Specular);
    glLightfv(GL_LIGHT0,GL_POSITION,Position);
    //  Set materials

    glMaterialfv(GL_FRONT_AND_BACK,GL_SHININESS,Shinyness);
    glMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,Specular);
    glMaterialfv(GL_FRONT_AND_BACK,GL_EMISSION,Emission);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE,GL_TRUE);
    if(sphere)
    {
      glPushMatrix();
      //  Offset, scale and rotate
      glTranslated(0.3,0,0);
      glScaled(1,1,1);
      //  White ball
      glColor3f(1,0.5,1);
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      glutSolidSphere(0.3,40,40);
      //  Undo transofrmations
      glPopMatrix();
    }

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    // Draw cloth
      glBindTexture(GL_TEXTURE_2D, texture[0]);

    glUseProgram(shader[0]);

    int position_vbo = cloth.getVBO(0);
    int normal_vbo = cloth.getVBO(1);
    glBindBuffer(GL_ARRAY_BUFFER, position_vbo);
    glVertexPointer(4, GL_FLOAT, 0, 0);
    glBindBuffer(GL_ARRAY_BUFFER, normal_vbo);
    glNormalPointer(GL_FLOAT, sizeof(float)*4, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glTexCoordPointer(2, GL_FLOAT, sizeof(float) * 2, &(cloth.texture_coordinates[0]));
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    if (wireframe_mode)
    {
        glColor3f(1.f, 1.f, 1.f);
        glEnable(GL_POLYGON_OFFSET_LINE);
        glPolygonOffset(-1.0f, -1.0f);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glDrawElements(GL_TRIANGLES, (GLsizei)cloth.tri_indices.size(), GL_UNSIGNED_INT, &(cloth.tri_indices[0]));
        glDisable(GL_POLYGON_OFFSET_LINE);
    }
    else
    {
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      glDrawElements(GL_TRIANGLES, (GLsizei)cloth.tri_indices.size(), GL_UNSIGNED_INT, &(cloth.tri_indices[0]));
    }

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glUseProgram(0);
    //glDisable(GL_TEXTURE_2D);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisable(GL_LIGHTING);

   glColor3f(1,1,1);
   if (axes)
   {
      glBegin(GL_LINES);
      glVertex3d(0.0,0.0,0.0);
      glVertex3d(len,0.0,0.0);
      glVertex3d(0.0,0.0,0.0);
      glVertex3d(0.0,len,0.0);
      glVertex3d(0.0,0.0,0.0);
      glVertex3d(0.0,0.0,len);
      glEnd();
      //  Label axes
      glRasterPos3d(len,0.0,0.0);
      Print("X");
      glRasterPos3d(0.0,len,0.0);
      Print("Y");
      glRasterPos3d(0.0,0.0,len);
      Print("Z");
    }

    // Calculate the FPS
    Frames++;
    {
      GLint t = glutGet(GLUT_ELAPSED_TIME);
      if (t - T0 >= 5000)
      {
        GLfloat seconds = (t - T0) / 1000.0;
        fps = Frames / seconds;
        //printf("%d frames in %6.3f seconds = %6.3f FPS\n", Frames, seconds, fps);
        T0 = t;
        Frames = 0;
      }
    }

    glColor3f(1,1,1);
    glWindowPos2i(5, 70);
    if (fps>0) Print("FPS %.3f", fps);

    // Display parameters and frame rate
    glWindowPos2i(5, 5);
    Print("cloth dim = %dx%d, compute(cpu=0, gpu=1) = %d", cloth_width, cloth_height, mode);
    glWindowPos2i(5, 45);
    Print("num iteration = %d, stiffness = %.f, damping = %.2f, inverse_mass = %.4f", sim_iter, stiffness, damping, inverse_mass);

    //  Render the scene and make it visible
    ErrCheck("display");
    glFlush();
    glutSwapBuffers();
}

void reshape(int width, int height)
{
   //  Ratio of the width to the height of the window
   asp = (height>0) ? (double)width/height : 1;
   //  Set the viewport to the entire window
   glViewport(0,0, width,height);
   //  Set projection
   Project(proj?fov:0,asp,dim);
}

void special(int key, int x, int y)
{
  //  Right arrow key - increase angle by 5 degrees
  if (key == GLUT_KEY_RIGHT)
    th += 5;
  //  Left arrow key - decrease angle by 5 degrees
  else if (key == GLUT_KEY_LEFT)
    th -= 5;
  //  Up arrow key - increase elevation by 5 degrees
  else if (key == GLUT_KEY_UP)
    ph += 5;
  //  Down arrow key - decrease elevation by 5 degrees
  else if (key == GLUT_KEY_DOWN)
    ph -= 5;
  //  PageUp key - increase dim
  else if (key == GLUT_KEY_PAGE_DOWN)
    dim += 0.1;
  //  PageDown key - decrease dim
  else if (key == GLUT_KEY_PAGE_UP && dim>1)
    dim -= 0.1;
  //  Keep angles to +/-360 degrees
  th %= 360;
  ph %= 360;
  //  Update projection
  Project(proj?fov:0,asp,dim);
  //  Tell GLUT it is necessary to redisplay the scene
  glutPostRedisplay();
}

void key(unsigned char ch, int x, int y)
{
    // Exit on escape
    if(ch == 27)
        exit(0);
    // Reset the view angle and mode
    else if(ch == '0')
    {
        th = -30;
        ph = 10;
        mode = 0;
        axes = 1;
        stiffness = 70000;
        damping = 0.5;
        sim_iter = 4;
        cloth_width = 16;
        cloth_height = 16;
        inverse_mass = inverse_mass_per_particle/(32.0*32.0);
        cloth.CreateCloth();
    }
    // Change mode CPU vs GPU Shader
    else if(ch == 'm' || ch == 'M') // Change the compute platform
    {
        mode = 1 - mode;
        // Create the cloth again
        cloth.CreateCloth();
    }
    // Decrease stiffness
    else if(ch == 's')
    {
        if(stiffness <= 40000)
        {
            stiffness = 40000;
        }
        else
            stiffness -= 5000;
    }
    else if(ch == 'S') // Increase the stiffness of the cloth
    {
        if(stiffness >= 100000)
        {
            stiffness = 100000;
        }
        else
            stiffness += 5000;
    }
    else if(ch == 32) // space bar to pause the simulation
    {
      pause_sim = 1 - pause_sim;
    }
    else if(ch == 'w' || ch == 'W') // Display wire frame (not working)
    {
      wireframe_mode = 1 - wireframe_mode;
    }
    else if(ch == 'v' || ch == 'V') // pause light source moving
    {
      move = 1 - move;
    }
    else if(ch == 'd') // Decrement the size of the cloth
    {
      if(cloth_width <= 4)
        cloth_width = 4;
      else
        cloth_width = cloth_width / 2;
      if(cloth_height <= 4)
        cloth_height = 4;
      else
        cloth_height = cloth_height / 2;

      if(cloth_height <= 16)
        inverse_mass = inverse_mass_per_particle/(32.0*32.0);
      else
        inverse_mass = inverse_mass_per_particle/float(cloth_width*cloth_width);

      cloth.CreateCloth();
    }
    else if(ch == 'D') // Incrememnt the size of the cloth
    {
      if(cloth_width >= 128)
        cloth_width = 128;
      else
        cloth_width = cloth_width * 2;

      if(cloth_height >= 128)
        cloth_height = 128;
      else
        cloth_height = cloth_height * 2;

      inverse_mass = inverse_mass_per_particle/float(cloth_width*cloth_width);

      cloth.CreateCloth();

    }
    else if(ch == 'i')
    {
      if(sim_iter <= 2)
      {
        sim_iter = 2;
      }
      else
      {
        sim_iter -= 2;
      }
    }
    else if(ch == 'I')
    {
      if(sim_iter > 20)
      {
        sim_iter = 20;
      }
      else
      {
        sim_iter += 2;
      }
    }
    else if(ch == 'a' || ch == 'A')
    {
      axes = 1 - axes;
    }
    else if(ch == 'c' || ch == 'C')
    {
      sphere = 1 - sphere;
      cloth.CreateCloth();
    }

   //  Reproject
   Project(proj?fov:0,asp,dim);

   //  Tell GLUT it is necessary to redisplay the scene
   glutPostRedisplay();

}

void step_simulation()
{
  if(pause_sim == 0)
  {
    for(int i = 0; i < sim_iter; ++i)
    {
      if(mode == 0)
      {
        cloth.SimulateClothPhysicsCpu();
      }
      else
      {
        cloth.SimulateClothPhysicsGpu();
      }
    }
  }

}

void idle()
{
  //  Elapsed time in seconds
  //  Tell GLUT it is necessary to redisplay the scene

  static double t0 = -1.;
  double dt, t = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
  if (move) zh = fmod(90*t,360.0);

  if (t0 < 0.0)
    t0 = t;
  dt = t - t0;
  t0 = t;

  step_simulation();

  glutPostRedisplay();
}

/*
 *  Print Shader Log
 */
void PrintShaderLog(int obj, const char* file)
{
   int len=0;
   glGetShaderiv(obj,GL_INFO_LOG_LENGTH,&len);
   if (len>1)
   {
      int n=0;
      char* buffer = (char *)malloc(len);
      if (!buffer) Fatal("Cannot allocate %d bytes of text for shader log\n",len);
      glGetShaderInfoLog(obj,len,&n,buffer);
      fprintf(stderr,"%s:\n%s\n",file,buffer);
      free(buffer);
   }
   glGetShaderiv(obj,GL_COMPILE_STATUS,&len);
   if (!len) Fatal("Error compiling %s\n",file);
}

// Read text file to read shaders
char* ReadText(const char *file)
{
   int   n;
   char* buffer;
   //  Open file
   FILE* f = fopen(file,"rt");
   if (!f) Fatal("Cannot open text file %s\n",file);
   //  Seek to end to determine size, then rewind
   fseek(f,0,SEEK_END);
   n = ftell(f);
   rewind(f);
   //  Allocate memory for the whole file
   buffer = (char*)malloc(n+1);
   if (!buffer) Fatal("Cannot allocate %d bytes for text file %s\n",n+1,file);
   //  Snarf the file
   if (fread(buffer,n,1,f)!=1) Fatal("Cannot read %d bytes for text file %s\n",n,file);
   buffer[n] = 0;
   //  Close and return
   fclose(f);
   return buffer;
}

void PrintProgramLog(int obj)
{
   int len=0;
   glGetProgramiv(obj,GL_INFO_LOG_LENGTH,&len);
   if (len>1)
   {
      int n=0;
      char* buffer = (char *)malloc(len);
      if (!buffer) Fatal("Cannot allocate %d bytes of text for program log\n",len);
      glGetProgramInfoLog(obj,len,&n,buffer);
      fprintf(stderr,"%s\n",buffer);
   }
   glGetProgramiv(obj,GL_LINK_STATUS,&len);
   if (!len) Fatal("Error linking program\n");
}


// Create shader
int CreateShader(GLenum type, const char* file)
{
   //  Create the shader
   int shader = glCreateShader(type);
   //  Load source code from file
   char* source = ReadText(file);
   glShaderSource(shader,1,(const char**)&source,NULL);
   free(source);
   //  Compile the shader
   fprintf(stderr,"Compile %s\n",file);
   glCompileShader(shader);
   //  Check for errors
   PrintShaderLog(shader,file);
   //  Return name
   return shader;
}

// Create Shader Program
int CreateShaderProg(const char* VertFile, const char* FragFile)
{
   //  Create program
   int prog = glCreateProgram();
   //  Create and compile vertex shader
   int vert = CreateShader(GL_VERTEX_SHADER, VertFile);
   //  Create and compile fragment shader
   int frag = CreateShader(GL_FRAGMENT_SHADER, FragFile);
   //  Attach vertex shader
   glAttachShader(prog,vert);
   //  Attach fragment shader
   glAttachShader(prog,frag);
   //  Link program
   glLinkProgram(prog);
   //  Check for errors
   PrintProgramLog(prog);
   //  Return name
   return prog;
}

int main(int argc, char** argv)
{
    // Initialize GLUT
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(700, 700);
    glutCreateWindow("Cloth Simulation: Sharad Bade");

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutSpecialFunc(special);
    glutKeyboardFunc(key);
    glutIdleFunc(idle);

    //  Load texture
    texture[0] = LoadTexBMP("chelsea.bmp");

    shader[0] = CreateShaderProg("cloth.vert", "cloth.frag");
    shader[1] = CreateShaderProg("simulation.vert", "simulation.frag");
    cloth.CreateCloth();
    ErrCheck("init");

    glutMainLoop();
    return 0;
}
