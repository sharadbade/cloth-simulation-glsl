/*
Copyright (c) 2016, Sharad Bade
All rights reserved.
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list
of conditions and the following disclaimer in the documentation and/or other
materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.
*/

#ifndef CLOTH_H
#define CLOTH_H

#define GL_GLEXT_PROTOTYPES

#ifdef __APPLE__
    #include <GLUT/glut.h>
    #include <OpenGL/gl.h>
#else
    #include <GL/glut.h>
    #include <GL/gl.h>
#endif

#include "vector.h"
#include <vector>

// defined in main.cpp
extern float stiffness;
extern float damping;
extern float inverse_mass;
extern int cloth_width;
extern int cloth_height;
extern int mode;
extern float time_step;
extern int shader[2];
extern int fbo_select;
extern unsigned int texture[2];
extern int sphere;

class Cloth
{
    public:
        // Default no argument constructor
        Cloth();

        // Destructor
        ~Cloth();

        // Create cloth
        void CreateCloth();

        // Create and initialize the vertex buffer objects
        void CreateVertexBufferObjects();

        // Frame buffer objects for current and previous positions
        struct my_fbo
        {
            // current position, previous positions, normals to be attached to fbo
            std::vector<int> textures;

            // frame buffer id
            unsigned id;

            // width of the fbo
            unsigned w;

            // height of the fbo
            unsigned h;

            my_fbo(int arg_w, int arg_h) { w = arg_w; h = arg_h; }
        };

        inline int getVBO(int i) { return vertex_buffer_object[i]; }

        inline std::vector<float>& getTexCoord() { return texture_coordinates; }

        inline std::vector<int>& getTriIndices() { return tri_indices; }

        // Create and initialize frame buffer objects
        my_fbo* CreateFrameBufferObjects(int cloth_dim, std::vector<float*> v);

        // Select the frame buffer object for ping-pong technique
        void SelectFrameBufferObject(int select);

        // Simulate the cloth physics using CPU
        void SimulateClothPhysicsCpu();

        // Simulate the cloth physics using GPU
        void SimulateClothPhysicsGpu();

        // texture coordinates
        std::vector<float> texture_coordinates;

        // triangle indices
        std::vector<int> tri_indices;


    private:
        // prevent copy constructor
        Cloth(const Cloth& arg);

        // prevent assignement operator
        Cloth& operator=(const Cloth& arg);

        // current positions of all the points on the cloth
        std::vector<float> current_positions;

        // previous positions of all the points on the cloth
        std::vector<float> previous_positions;

        // normals associated with every point on the cloth
        std::vector<float> current_normals;

        // Vertex buffer objects for position and normals
        unsigned vertex_buffer_object[2];

        // store adjacent point positions
        struct adjacent_point
        {
            int i; int j;
            adjacent_point():i(0), j(0){}
            adjacent_point(int i_, int j_){ i = i_; j = j_; }
        };

        // Vector storing the positions of the adjacent particles
        std::vector<adjacent_point> adjacent_particle;

        // ping pong
        // 1st frame buffer which will hold position and normal textures from fbo2
        std::vector<my_fbo*> fbo_vec;

        // 2nd frame buffer which will hold position and normal textures from fbo1
        //my_fbo* fbo2;
};

#endif
