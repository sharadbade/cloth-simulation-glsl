/*
Copyright (c) 2016, Sharad Bade
All rights reserved.
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list
of conditions and the following disclaimer in the documentation and/or other
materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.
*/

#ifndef VECTOR_H
#define VECTOR_H

#include <math.h>
#include <iostream>

// Structure to define a 3d vector
struct Vector_3
{
    // Vector componenets
    float x;
    float y;
    float z;

    // Default constructor
    Vector_3();

    // Copy constructor
    Vector_3(const Vector_3& arg);

    // Assignment operator
    Vector_3& operator=(const Vector_3& arg);

    // Argument constructor
    Vector_3(const float arg_x, const float arg_y, const float arg_z);

    // Length of the vector
    float Len();

    // Calculate Unit vector
    Vector_3 UnitVec();

    // Dot product operator
    float operator*(const Vector_3& arg);

    // Cross product operator
    Vector_3 operator^(const Vector_3& arg);

    // Scalar product operator
    Vector_3 operator*(float scalar);

    // Add two vectors
    Vector_3 operator+(const Vector_3& arg);

    // Subtract two vectors
    Vector_3 operator-(const Vector_3& arg);

    // Devide by scalar
    Vector_3 operator/(const float arg);

    // += operator
    void operator+=(const Vector_3& arg);

    friend std::ostream& operator<<(std::ostream& os, const Vector_3& vec)
    {
        os << ": x=" <<vec.x<<", y="<<vec.y<<", z="<<vec.z;
        return os;
    }
};

#endif // VECTOR_H