/*
Copyright (c) 2016, Sharad Bade
All rights reserved.
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list
of conditions and the following disclaimer in the documentation and/or other
materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.
*/

uniform sampler2D	tex1;
uniform sampler2D	tex2;
uniform int sphere;
uniform float cloth_dim;
uniform float stiffness;
uniform float damping;
uniform float time_step;
uniform float inv_mass;

// adjacent points
vec2 points[8];

void main(void)
{
	// initialize the positions of the adjacent points, excluding current point 0,0
	points[0] = vec2(-1, -1);
	points[1] = vec2(-1, 0);
	points[2] = vec2(-1, 1);
	points[3] = vec2(0, 1);
	points[4] = vec2(1, 1);
	points[5] = vec2(1, 0);
	points[6] = vec2(1, -1);
	points[7] = vec2(0, -1);

	// Read the current positions of the particle from texture
	vec3 curr_pos = texture2D(tex1, gl_TexCoord[0].xy).xyz;
	// Read the previous positions of the particle from texture
	vec3 prev_pos = texture2D(tex2, gl_TexCoord[0].xy).xyz;
	// Calculate the velocity using for verlet equations
	vec3 velocity = (curr_pos - prev_pos) / time_step;
	// initialize normal
	vec3 normal = vec3(0, 0, 0);
	// initialize gravity
	vec3 gravity = vec3(0, -9.81, 0);
	// store the vector connecting particles when they are in stretched position
	vec3 prev_stretched_vector = vec3(0, 0, 0);
	// number of adjacent particles
	float adj_num_particles = 0.0;
	// normalize the poistions to cloth dimensions
	float step = 1.0 / (cloth_dim - 1.0);
	// number of cloumns in cloth particle mesh
	float column = floor(gl_TexCoord[0].x * cloth_dim);
	// number of rows in cloth particle mesh
	float row = floor(gl_TexCoord[0].y * cloth_dim);
	// array index of the particles
	float index = row * cloth_dim + column;
	// store the resultant force, initialize it with gravity
	vec3 resultant_force = vec3(0, -9.81, 0);
	// store inverse mas
	float inverse_mass = inv_mass;
	// is the index going out of cloth particle mesh dimensions
	if (index <= (cloth_dim - 1.0))
		inverse_mass = 0.0;

	// Iterate through all the adjacent particles which will exter force upon
	// current particle
	for (int c = 0; c < 8; ++c)
	{
		float j = points[c].x;
		float i = points[c].y;
		// Skip if out of cloth mesh bounds
		if (((row + i) < 0.0) ||
			((column + j) < 0.0) ||
			((row + i) > (cloth_dim - 1.0)) ||
			((column + j) > (cloth_dim - 1.0)))
			continue;
		// calculate the position of the adjacent particle
		vec2 adj_index = vec2(column + j, row + i) / cloth_dim;
		vec3 adj_pos = texture2D(tex1, adj_index).xyz;
 		vec3 temp_vec = adj_pos - curr_pos;
		vec3 stretch_vector = normalize(temp_vec);
		if (adj_num_particles > 0.0)
		{
			// angle between current stretch vector and previous
			float th = dot(stretch_vector, prev_stretched_vector);
			// calculate the normal using the angle
			if (th > 0.0) normal += cross(prev_stretched_vector, stretch_vector);
		}
		adj_num_particles += 1.0;
		prev_stretched_vector = stretch_vector;
		// rest length between two particles of a mesh, normalized to cloth dimensions
		float rest_length = length(points[c]) * step;
		// update the resultant spring force using velocity verlet method
		resultant_force += (stretch_vector * (length(temp_vec) - rest_length)) * stiffness - velocity * damping;
	}
	// normalize the normal
	normal = normalize(normal / (adj_num_particles));
	// acceleration using velocity verlet method
	vec3 acceleration = (resultant_force + gravity) * inverse_mass * 0.01;
	vec3 temp = curr_pos;
	// update the current psition of the particle using velocity verlet method
	curr_pos = curr_pos * 2.0 - prev_pos + acceleration * time_step * time_step;
	prev_pos = temp;

	if(sphere == 1)
	{
		vec3 c = vec3(0.3, 0, 0);
		float r = 0.31;
		if(length(curr_pos - c) < r)
		{
			vec3 collision = normalize(curr_pos - c);
			curr_pos = c + collision * r;
		}

	}
	// store the result of the computation from next simulation step
	gl_FragData[0] = vec4(curr_pos, 1.0);
	gl_FragData[1] = vec4(prev_pos,	1.0);
	gl_FragData[2] = vec4(normal,	0.0);
}
