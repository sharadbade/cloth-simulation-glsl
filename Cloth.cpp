/*
Copyright (c) 2016, Sharad Bade
All rights reserved.
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list
of conditions and the following disclaimer in the documentation and/or other
materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.
*/

#include "Cloth.h"

Cloth::Cloth()
{
    current_positions.clear();
    previous_positions.clear();
    current_normals.clear();
    texture_coordinates.clear();
    tri_indices.clear();

    fbo_vec.resize(2, NULL);

    vertex_buffer_object[0] = 0;
    vertex_buffer_object[1] = 0;

    adjacent_particle.push_back(adjacent_point(-1, -1));
    adjacent_particle.push_back(adjacent_point(-1, 0));
    adjacent_particle.push_back(adjacent_point(-1, 1));
    adjacent_particle.push_back(adjacent_point(0, 1));
    adjacent_particle.push_back(adjacent_point(1, 1));
    adjacent_particle.push_back(adjacent_point(1, 0));
    adjacent_particle.push_back(adjacent_point(1, -1));
    adjacent_particle.push_back(adjacent_point(0, -1));
}

Cloth::~Cloth()
{
    glDeleteBuffers(2, vertex_buffer_object);
}

void Cloth::CreateCloth()
{
    // clear all the vectors
    current_positions.clear();
    previous_positions.clear();
    current_normals.clear();
    texture_coordinates.clear();
    tri_indices.clear();

    glDeleteBuffers(2, vertex_buffer_object);
    if(fbo_vec[0])
        glDeleteFramebuffers(1, &(fbo_vec[0]->id));
    if(fbo_vec[1])
        glDeleteFramebuffers(1, &(fbo_vec[1]->id));
    fbo_vec[0] = NULL;
    fbo_vec[1] = NULL;

    // Assign initial particle current_positions
    for(int i = 0; i < cloth_height; ++i)
    {
        for(int j = 0; j < cloth_width; ++j)
        {
            //x
            current_positions.push_back( j / double(cloth_width - 1));
            //y
            current_positions.push_back( 1 );
            //z
            current_positions.push_back( i / double(cloth_height - 1));
            //w
            current_positions.push_back( 1 );

            // Indices for drawing traingle
            if(( i < cloth_height - 1) && (j < cloth_width - 1))
            {
                // x3.....x2
                // .     .
                // .   .
                // . .
                // x1
                // First triangle of the quad
                tri_indices.push_back(i * cloth_height + j); //x1
                tri_indices.push_back((i+1) * cloth_height + j + 1); //x2
                tri_indices.push_back((i+1) * cloth_height + j); //x3

                //        x3
                //      . .
                //   .    .
                // .      .
                // x1.....x2
                tri_indices.push_back(i * cloth_height + j); //x1
                tri_indices.push_back( i * cloth_height + j + 1); //x2
                tri_indices.push_back((i+1) * cloth_height + j + 1); //x3
            }
        }
    }

    previous_positions = current_positions;
    current_normals.assign(cloth_width * cloth_height * 4, float(0.0));
    // assign texture coordinates for the cloth
    for(int i = 0; i < cloth_width * cloth_height; ++i)
    {
        texture_coordinates.push_back(current_positions[i*4]);
        texture_coordinates.push_back(current_positions[i*4 + 2]);
    }

    // Create vertext buffer objects
    CreateVertexBufferObjects();

    if (mode == 1)
    {
        std::vector<float*> v;
        v.push_back(&current_positions[0]);
        v.push_back(&previous_positions[0]);
        v.push_back(&current_normals[0]);
        fbo_vec[0] = CreateFrameBufferObjects(cloth_width, v);
        fbo_vec[1] = CreateFrameBufferObjects(cloth_width, v);
    }

}

void Cloth::CreateVertexBufferObjects()
{
    glGenBuffers(2, vertex_buffer_object);

    // vbo for positions
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object[0]);
    glBufferData(GL_ARRAY_BUFFER, cloth_width * cloth_width * 4 * sizeof(float), 0, GL_DYNAMIC_DRAW);
    // vbo for normals
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object[1]);
    glBufferData(GL_ARRAY_BUFFER, cloth_width * cloth_width * 4 * sizeof(float), 0, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

Cloth::my_fbo* Cloth::CreateFrameBufferObjects(int size, std::vector<float*> v)
{
    Cloth::my_fbo* temp_fbo = new my_fbo(size, size);

    // intialize textures to attach to the fbo
    // one each for current_positions, previous_positions and current_normals
    for(int i = 0; i < v.size(); ++i)
    {
        unsigned id;
        glGenTextures(1, &id);
        glBindTexture(GL_TEXTURE_2D, id);
        glPixelStorei(GL_UNPACK_ALIGNMENT,1);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size, size, 0, GL_RGBA, GL_FLOAT, v[i]);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        temp_fbo->textures.push_back(id);
    }

    // generate frame buffer objects
    glGenFramebuffers(1, &temp_fbo->id);
    glBindFramebuffer(GL_FRAMEBUFFER, temp_fbo->id);

    // attach three textures to frame buffer objects
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, temp_fbo->textures[0], 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, temp_fbo->textures[1], 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, temp_fbo->textures[2], 0);

    // Bind default frame buffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    GLint location;

    glUseProgram(shader[1]);
    location = glGetUniformLocation(shader[1], "tex1");
    glUniform1i(location, 0); // load texture unit 0 in tex1
    location = glGetUniformLocation(shader[1], "tex2");
    glUniform1i(location, 1); // load texture unit 1 in tex2
    // load the simulation parameters in the GLSL fragment shader
    location = glGetUniformLocation(shader[1], "cloth_dim");
    glUniform1f(location, cloth_width);
    location = glGetUniformLocation(shader[1], "time_step");
    glUniform1f(location, time_step);
    location = glGetUniformLocation(shader[1], "damping");
    glUniform1f(location, damping);
    location = glGetUniformLocation(shader[1], "stiffness");
    glUniform1f(location, stiffness);
    location = glGetUniformLocation(shader[1], "inv_mass");
    glUniform1f(location, inverse_mass);
    location = glGetUniformLocation(shader[1], "sphere");
    glUniform1i(location, sphere);
    glUseProgram(0);

    return temp_fbo;
}

void Cloth::SimulateClothPhysicsCpu()
{
    int num_particles = cloth_height * cloth_width;
    Vector_3 gravity(0, -9.81, 0);
    std::vector<float> next_positions;
    next_positions.assign(num_particles * 4, 0.0);
    float inv_mass = 0;
    for(int index = 0; index < num_particles; ++index)
    {
        Vector_3 curr_pos(current_positions[index * 4],
                     current_positions[index * 4 + 1],
                     current_positions[index * 4 + 2]);
        Vector_3 prev_pos(previous_positions[index * 4],
                         previous_positions[index * 4 + 1],
                         previous_positions[index * 4 + 2]);
        Vector_3 velocity = (curr_pos - prev_pos) / time_step;

        Vector_3 normal(0, 0, 0);
        inv_mass = inverse_mass;
        if((index == 0) || (index <= (cloth_width - 1)))
        {
            inv_mass = 0.f;
        }
        int column = index % cloth_width;
        int row = index / cloth_width;
        Vector_3 pos_rest(column / (float)(cloth_width - 1), 0, row / (float)(cloth_width - 1));
        Vector_3 prev_stretched_vector(0, 0, 0);
        Vector_3 resultant_force(0, 0, 0);
        // Iterate over nighbouring particles
        int adj_num_particle = 0;
        for(std::vector<adjacent_point>::iterator it = adjacent_particle.begin(); it != adjacent_particle.end(); ++it)
        {
            // skip if particle is out of cloth mesh dimension
            int i = it->i;
            int j = it->j;
            if(  ((row + i) < 0) ||
                 ((column + j) < 0) ||
                 ((row + i) > (cloth_width - 1)) ||
                 ((column + j) > (cloth_width - 1)))
            {
                    continue;
            }
            // get the position of the adjacent paticle
            int adj_index = (row + i) * cloth_width + column + j;
            Vector_3 adj_pos(current_positions[adj_index * 4],
                              current_positions[adj_index * 4 + 1],
                              current_positions[adj_index * 4 + 2]);
            Vector_3 adj_rest((column + j) / (float)(cloth_width - 1),
                               0,
                               (row + i) / (float)(cloth_width - 1));
            // calculate the rest length between current and adjacent particle
            Vector_3 rest_length = adj_rest - pos_rest;
            //calculate stretched length between two points
            Vector_3 stretched_length = adj_pos - curr_pos;
            // Unit vector along stretched length
            Vector_3 stretch_vector = stretched_length;
            stretch_vector.UnitVec();
            // Calculate the angle between previous direction of motion and
            // current direction of center particle
            if(it != adjacent_particle.begin())
            {
                // get the angle between previous and current strech vector
                float th = stretch_vector * prev_stretched_vector;
                // Calculate the resultant normal
                if(th > 0) normal += stretch_vector ^ prev_stretched_vector;
                ++adj_num_particle;
            }
            prev_stretched_vector = stretch_vector;
            // Calculate spring force due to the current adjacent particle
            float diff_length = stretched_length.Len() - rest_length.Len();
            resultant_force += (stretch_vector * stiffness) * diff_length - velocity * damping;
        }

        // normaize the normal
        normal = (normal / (float)(adj_num_particle)).UnitVec();

        //Use verlet method integration equations
        Vector_3 acc = (resultant_force + gravity) *  inv_mass;
        Vector_3 tmp = curr_pos;
        curr_pos = curr_pos * 2 - prev_pos + acc * time_step * time_step;
        //alternate formula for verlet
        //curr_pos = (vel + acc * time_step /2) * time_step;
        prev_pos = tmp;

        if(sphere)
        {
            Vector_3 c(0.3, 0, 0);
            float r = 0.31;
            if((curr_pos - c).Len() < r)
            {
                Vector_3 collision = (curr_pos - c).UnitVec();
                curr_pos = c + collision * r;
            }
        }

        next_positions[index * 4] = curr_pos.x;
        next_positions[index * 4 + 1] = curr_pos.y;
        next_positions[index * 4 + 2] = curr_pos.z;
        previous_positions[index * 4] = prev_pos.x;
        previous_positions[index * 4 + 1] = prev_pos.y;
        previous_positions[index * 4 + 2] = prev_pos.z;
        current_normals[index * 4] = normal.x;
        current_normals[index * 4 + 1] = normal.y;
        current_normals[index * 4 + 2] = normal.z;
    }

    // Make next positions as current
    for (int i = 0; i < num_particles; i++)
    {
        current_positions[i * 4] = next_positions[i * 4];
        current_positions[i * 4 + 1] = next_positions[i * 4 + 1];
        current_positions[i * 4 + 2] = next_positions[i * 4 + 2];
    }
    unsigned int size = cloth_width * cloth_width * 4 * sizeof(float);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object[0]);
    glBufferData(GL_ARRAY_BUFFER, size, (GLvoid*)&(current_positions[0]), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object[1]);
    glBufferData(GL_ARRAY_BUFFER, size, (GLvoid*)&(current_normals[0]), GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Cloth::SelectFrameBufferObject(int select)
{
    // Bind one of the frame buffers selected by "select"
    if(fbo_vec[select] != NULL)
        glBindFramebuffer(GL_FRAMEBUFFER, fbo_vec[select]->id);
    else
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

    // Attached colors/data to the frame buffers
    GLenum color_attachements[] = { GL_COLOR_ATTACHMENT0,// current positions
                                    GL_COLOR_ATTACHMENT1, // previous positions
                                    GL_COLOR_ATTACHMENT2}; // normals
    // Dump data generated by shader into current frame buffer attachements
    glDrawBuffers(3, color_attachements);
    // Prepare next frame buffer as a input the shader
    if(fbo_vec[1-select] != NULL)
    {
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, fbo_vec[1-select]->textures[2]);//normals
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, fbo_vec[1-select]->textures[1]);//previous positions
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, fbo_vec[1-select]->textures[0]);// current positions
    }
    else
    {
        glBindTexture(GL_TEXTURE_2D, 0);
    }

}
void Cloth::SimulateClothPhysicsGpu()
{
    // Grab the current view port dimensions for later use
    int viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();

    // View port size is the cloth size
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0, (GLfloat) cloth_width, 0.0, (GLfloat) cloth_height);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glViewport(0, 0, cloth_width, cloth_height);

    SelectFrameBufferObject(fbo_select);
    // Draw the outline of the cloth using vertex shader
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glUseProgram(shader[1]);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex2i(0, 0);
    glTexCoord2f(0, 1);
    glVertex2i(0, (GLint)cloth_height);
    glTexCoord2f(1, 1);
    glVertex2i((GLint)cloth_width, (GLint)cloth_height);
    glTexCoord2f(1, 0);
    glVertex2i((GLint)cloth_width, 0);
    glEnd();

    glUseProgram(0);

    glReadBuffer(GL_COLOR_ATTACHMENT0);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, vertex_buffer_object[0]);
    glReadPixels(0, 0, cloth_height, cloth_width, GL_RGBA, GL_FLOAT, 0);

    glReadBuffer(GL_COLOR_ATTACHMENT2);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, vertex_buffer_object[1]);
    glReadPixels(0, 0, cloth_height, cloth_width, GL_RGBA, GL_FLOAT, 0);

    glReadBuffer(GL_NONE);
    glBindBuffer(GL_PIXEL_PACK_BUFFER, 0 );

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //restore viewport viewport[2] = width, viewport[3]= height
    gluOrtho2D(0.0, (GLfloat) viewport[2], 0.0, (GLfloat) viewport[3]);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glViewport(0, 0, viewport[2], viewport[3]);

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    fbo_select = 1 - fbo_select;
}






