/*
Copyright (c) 2016, Sharad Bade
All rights reserved.
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list
of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list
of conditions and the following disclaimer in the documentation and/or other
materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.
*/

#include "vector.h"


// Default construcor
Vector_3::Vector_3():x(0), y(0), z(0) {}

// Copy constructor
Vector_3::Vector_3(const Vector_3& arg)
{
    x = arg.x;
    y = arg.y;
    z = arg.z;
}

// Assignement operator
Vector_3& Vector_3::operator=(const Vector_3& arg)
{
    x = arg.x;
    y = arg.y;
    z = arg.z;

    return *this;
}

// Copy constructor
Vector_3::Vector_3(const float arg_x, const float arg_y, const float arg_z)
{
    x = arg_x;
    y = arg_y;
    z = arg_z;
}

float Vector_3::Len()
{
    return sqrt(x*x + y*y + z*z);
}

Vector_3 Vector_3::UnitVec()
{
    float curr_length = Len();
    x = x / curr_length;
    y = y / curr_length;
    z = z / curr_length;

    return *this;
}

float Vector_3::operator*(const Vector_3& arg)
{
    return (x*arg.x + y*arg.y + z*arg.z);
}

Vector_3 Vector_3::operator*(float scalar)
{
    return Vector_3(x*scalar, y*scalar, z*scalar);
}

Vector_3 Vector_3::operator^(const Vector_3& arg)
{
    return Vector_3(y * arg.z - z * arg.y, z * arg.x - x * arg.z, x * arg.y - y * arg.x);
}

Vector_3 Vector_3::operator+(const Vector_3& arg)
{
    return Vector_3
    (
        x + arg.x,
        y + arg.y,
        z + arg.z
    );
}

Vector_3 Vector_3::operator-(const Vector_3& arg)
{
    return Vector_3
    (
        x - arg.x,
        y - arg.y,
        z - arg.z
    );
}

Vector_3 Vector_3::operator/(const float arg)
{
    return Vector_3
    (
        x / arg,
        y / arg,
        z / arg
    );
}

void Vector_3::operator+=(const Vector_3& arg)
{
    x += arg.x;
    y += arg.y;
    z += arg.z;
}

