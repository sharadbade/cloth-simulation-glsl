EXE=run

# Main Target
all : $(EXE)

#  MinGW
ifeq "$(OS)" "Windows_NT"
CFLG=-O3 -Wall
LIBS=-lglut32cu -lglu32 -lopengl32
CLEAN=del *.exe *.o *.a
else
#  OSX
ifeq "$(shell uname)" "Darwin"
CFLG=-Wall -Wno-deprecated-declarations
LIBS=-framework GLUT -framework OpenGL
#  Linux/Unix/Solaris
else
CFLG=-O3 -Wall
LIBS=-lglut -lGLU -lGL -lm
endif
#  OSX/Linux/Unix/Solaris
CLEAN=rm -f $(EXE) *.o *.a
endif

# Dependecies
main.o: main.cpp CSCIx229.h
vector.o: vector.cpp vector.h
Cloth.o: Cloth.cpp Cloth.h
project.o: project.cpp CSCIx229.h
errcheck.o: errcheck.cpp CSCIx229.h
loadtexbmp.o: loadtexbmp.cpp CSCIx229.h
fatal.o: fatal.cpp CSCIx229.h
print.o: print.cpp

# Create archives
helper.a: vector.o Cloth.o errcheck.o loadtexbmp.o fatal.o project.o print.o
	ar -rcs $@ $^

# Compile rules
.c.o:
	gcc -c -g $(CFLG) $<
.cpp.o:
	g++ -c -g $(CFLG) $<

# Link
run:main.o helper.a
	g++ -g -o $@ $^ $(LIBS)

clean:
	$(CLEAN)
