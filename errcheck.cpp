/* This code is taken from UC Boulder's CS-5229 course
*/

/*
 *  Check for OpenGL errors
 */
#include "CSCIx229.h"

void ErrCheck(const char* where)
{
   int err = glGetError();
   if (err) fprintf(stderr,"ERROR: %s [%s]\n",gluErrorString(err),where);
}
